from unittest import TestCase
from .GutenBook import GutenBook
import requests

titles_list = [
    {
        'title': 'Pride and Prejudice',
        'url_from_title': 'https://gutendex.com/books/?search=pride%20and%20prejudice',
        'id': 1342,
        'first_name': 'Jane',
        'last_name': 'Austen',
        'birth_year': 1775,
        'death_year': 1817,
        'text_url': "https://www.gutenberg.org/files/1342/1342-0.txt"
    }
]


class TestGutenBook(TestCase):

    def test_init(self):

        for title in titles_list:
            guten = GutenBook(title['title'])
            guten.get_response_from_guten()
            print('url for this title', guten.url_from_title)
            self.assertEqual(title['title'], guten.title)
            self.assertEqual(title['url_from_title'], guten.url_from_title)

            data = guten.get_response_from_guten().json()
            guten.get_information_about_author()
            guten.get_url_with_text()
            results = data['results'][0]
            print('RESULTS: ', results)

            self.assertEqual('Pride and Prejudice', title['title'])
            self.assertEqual(title['id'], results['id'])
            self.assertEqual(guten.author['first_name'], title['first_name'])
            self.assertEqual(guten.author['last_name'], title['last_name'])
            self.assertEqual(guten.author['birth_year'], title['birth_year'])
            self.assertEqual(guten.author['death_year'], title['death_year'])
            self.assertEqual(guten.text_url, title['text_url'])

            print('URL for TEXT', guten.text_url)
            print('there should be text of a book\n', guten.get_text_from_url())
