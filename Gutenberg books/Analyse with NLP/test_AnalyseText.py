import string
from unittest import TestCase
from AnalyseText import AnalyseText

# counts words:
# https://design215.com/toolbox/wordlist.php

# I lower the case of the words in the test

list_tests = [
    {
        'text': "Hello, it's me, can you hear me?",
        'number words': 7,
        'number sentences': 1,
        'words': ['Hello', 'it', "'s", 'me', 'can', 'you', 'hear']
    },
    {
        'text':
        """
            My name is Ozymandias, King of Kings;
            Look on my Works, ye Mighty, and despair!
            Nothing beside remains. Round the decay
            Of that colossal Wreck, boundless and bare
            The lone and level sands stretch far away.
        """,
        'number words': 31,
        'number sentences': 3,
        'words': ['and', 'away', 'bare', 'beside', 'boundless', 'colossal', 'decay', 'despair', 'far', 'is',
                  'King', 'Kings', 'level', 'lone', 'Look', 'Mighty', 'My', 'name', 'Nothing', 'of', 'on', 'Ozymandias',
                  'remains', 'Round', 'sands', 'stretch', 'that', 'the', 'Works', 'Wreck', 'ye']

    }
]


class TestAnalyseText(TestCase):

    def test_number_of_words(self):
        for test in list_tests:
            analyse = AnalyseText(test['text'])
            print('my number: ', analyse.number_of_words)
            print('expected number: ', test['number words'])
            self.assertEqual(analyse.number_of_words, test['number words'])

    def test_list_of_words(self):
        for test in list_tests:
            analyse = AnalyseText(test['text'])
            list_to_test = [word.casefold() for word in test['words']]

            print('my analyse words', analyse.list_words)
            print('expected list: ', list_to_test)
            self.assertEqual(analyse.list_words, sorted(list_to_test))