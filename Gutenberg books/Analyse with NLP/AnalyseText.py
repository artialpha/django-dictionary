import string
from nltk.tokenize import sent_tokenize, word_tokenize
from nltk.corpus import stopwords
import nltk
nltk.download('punkt')
nltk.download("stopwords")


class AnalyseText:

    headers = {

    }

    '''
        Init function initialize a list of words of a text you give it
        There is something that is  called 'stopping words' like (eg 'me', 'can', 'and)
        To get rid of stopping words you must use method 'list_words_without_stopping_words'
    '''

    def __init__(self, text, language='english'):
        self.text = text
        self.stop_words = set(stopwords.words(language))

        self.list_words = None
        self.get_list_words()

        self.number_of_words = None
        self.get_number_of_words()

    def get_list_words(self):
        without_punctuation = [word.casefold() for word in word_tokenize(self.text) if word
                               not in list(string.punctuation)]
        without_repetition = set(without_punctuation)
        self.list_words = sorted(list(without_repetition))

    def get_number_of_words(self):
        self.number_of_words = len(self.list_words)

    def list_words_without_stopping_words(self):
        self.list_words = [word for word in self.list_words if word not in self.stop_words]

    def words_with_frequency_zpf(self):
        pass

    def words_with_frequency_per_million(self):
        pass

    def get_number_of_sentences(self):
        pass

    def get_frequency_word_in_text(self):
        pass

    def zipf_of_words(self):
        pass
