import requests
import urllib


class GutenBook:

    guten_url = 'https://gutendex.com/books/'

    def __init__(self, title):
        self.title = title
        self.url_from_title = f'{self.guten_url}?search={urllib.parse.quote(title.lower())}'

        self.response = None
        self.data_json = None
        self.get_response_from_guten()

        self.author = {}
        self.get_information_about_author()

        self.text_url = None
        self.get_url_with_text()

        self.encoding_from_json = None

    def get_response_from_guten(self):
        self.response = requests.get(self.url_from_title)
        self.data_json = self.response.json()['results'][0]
        return self.response

    def get_information_about_author(self):
        data_author = self.data_json['authors'][0]
        l, f = data_author['name'].split(', ')
        self.author['first_name'] = f
        self.author['last_name'] = l
        self.author['birth_year'] = data_author['birth_year']
        self.author['death_year'] = data_author['death_year']

    def get_url_with_text(self):
        try:
            self.text_url = self.data_json['formats']['text/plain']
        except KeyError:
            try:
                self.text_url = self.data_json['formats']['text/plain; charset=utf-8']
                self.encoding_from_json = 'utf-8'
            except KeyError:
                try:
                    self.text_url = self.data_json['formats']['text/plain; charset=us-ascii']
                    self.encoding_from_json = 'us-ascii'
                except KeyError:
                    try:
                        self.text_url = self.data_json['formats']['text/plain; charset=iso-8859-1']
                        self.encoding_from_json = 'iso-8859-1'
                    except KeyError:
                        raise Exception('no KEY!!!!')

    def get_text_from_url(self):
        def remove_preface(text):
            str_remove = f'*** START OF THE PROJECT GUTENBERG EBOOK {self.title.upper()} ***'
            index = text.find(str_remove)
            index += len(str_remove)
            print('INDEX', index)
            return text[index:]

        response_from_text_url = requests.get(self.text_url)
        response_from_text_url.encoding = self.encoding_from_json
        text_from_url = response_from_text_url.text

        return remove_preface(text_from_url)

