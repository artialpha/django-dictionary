from django.db import models
from django.urls import reverse
from django.utils.text import slugify
import pdfplumber
import datetime

YEARS = [(x, str(x)) for x in range(1500, datetime.datetime.now().year+1)]


class Author(models.Model):
    first_name = models.CharField(max_length=15)
    last_name = models.CharField(max_length=15)
    slug = models.SlugField(blank=True, null=True)
    nationality = models.CharField(max_length=20, null=True)
    date_of_birth = models.DateField()
    date_of_death = models.DateField(null=True, blank=True)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Book(models.Model):
    DATE_INPUT_FORMATS = ['%Y']

    author = models.ForeignKey('Author', on_delete=models.CASCADE)
    title = models.CharField(max_length=50)
    publication_date = models.IntegerField(choices=YEARS)
    book = models.FileField(null=True, blank=True)

    def save(self, *args, **kwargs):
        print(self.book)
        with pdfplumber.open(self.book) as pdf:
            first_page = pdf.pages[0]
            print('ilosc stron', len(pdf.pages))
            for page in pdf.pages:
                print(page.extract_text())
            print('ma dac strone', first_page.extract_text())

        super().save(*args, **kwargs)

    def __str__(self):
        return f'{self.title}'
