from django.test import TestCase
from library.models import Author
# Create your tests here.


class AuthorModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Author.objects.create(first_name='Franz', last_name='Kafka', slug='Franz-Kafka',
                              nationality='Czech', date_of_birth='1883-07-03', date_of_death='1924-06-03')

    def test_one_object(self):
        author = Author.objects.get(id=1)
        expected_first_name = f'{author.first_name}'
        self.assertEqual(expected_first_name, 'Franz')
        print(" I truly test it")
