from django.urls import path

from library.views import ListAuthor, DetailAuthor, ListBook, DetailBook

urlpatterns = [
    # path('<slug:slug>/', DetailAuthor.as_view(), name='author_detail'),
    path('author/', ListAuthor.as_view(), name='author_list'),
    path('author/<int:pk>/', DetailAuthor.as_view(), name='author_detail'),
    path('book/', ListBook.as_view(), name='book_list'),
    path('book/<int:pk>/', DetailBook.as_view(), name='book_detail'),
]
